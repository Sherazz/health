"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var app_component_1 = require("./app.component");
var autocomplete_1 = require("primeng/autocomplete");
var accordion_1 = require("primeng/accordion");
var animations_1 = require("@angular/platform-browser/animations");
var app_routes_1 = require("./Route/app.routes");
var router_1 = require("@angular/router");
var dashboard_component_1 = require("./Dashboard/dashboard.component");
var home_component_1 = require("./Home/home.component");
var sidenav_component_1 = require("./sidenav/sidenav.component");
var doctors_component_1 = require("./Doctors/doctors.component");
var profile_component_1 = require("./profile/profile.component");
var messages_1 = require("primeng/messages");
var message_1 = require("primeng/message");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            http_1.HttpModule,
            common_1.CommonModule, forms_1.FormsModule, autocomplete_1.AutoCompleteModule, accordion_1.AccordionModule, animations_1.BrowserAnimationsModule, router_1.RouterModule.forRoot(app_routes_1.routes), message_1.MessageModule, messages_1.MessagesModule
        ],
        declarations: [
            app_component_1.AppComponent, dashboard_component_1.DashboardComponent, home_component_1.HomeComponent, sidenav_component_1.SidenavComponent, doctors_component_1.DoctorsComponent, profile_component_1.ProfileComponent
        ],
        providers: [],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map