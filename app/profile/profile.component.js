"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ProfileComponent = (function () {
    function ProfileComponent() {
        this.currentUser = "";
    }
    ProfileComponent.prototype.ngOnInit = function () {
        var vm = this;
        this.currentUser = localStorage.getItem('currentUser');
        var Users = Parse.Object.extend("User");
        var query = new Parse.Query(Users);
        query.equalTo("objectId", this.currentUser);
        query.first({
            success: function (object) {
                var profilePhoto = object.get("ProfilePic");
                if (profilePhoto != undefined) {
                    document.images[0].src = profilePhoto._url;
                }
                vm.name = object.attributes.name;
                vm.age = object.attributes.Age;
                vm.gender = object.attributes.Gender;
                vm.email = object.attributes.email;
                vm.username = object.attributes.username;
                // Successfully retrieved the object.
            },
            error: function (error) {
                alert("Error: " + error.code + " " + error.message);
            }
        });
    };
    return ProfileComponent;
}());
ProfileComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'my-app',
        templateUrl: './profile.component.html'
        // styleUrls: ['../styles.css']
    }),
    core_1.Injectable(),
    __metadata("design:paramtypes", [])
], ProfileComponent);
exports.ProfileComponent = ProfileComponent;
//# sourceMappingURL=profile.component.js.map