import { Component, OnInit, Injectable } from '@angular/core';
import { CommonModule } from "@angular/common";

@Component({
  moduleId: module.id,
  selector: 'my-app',
  templateUrl: './profile.component.html'
  // styleUrls: ['../styles.css']
})
@Injectable()
export class ProfileComponent implements OnInit {
  currentUser: string = "";
  name: any;
  age: any;
  gender: any;
  email: any;
  username: any;
  constructor() { }

  ngOnInit() {
    const vm = this;
    this.currentUser = localStorage.getItem('currentUser');
    var Users = Parse.Object.extend("User");
    var query = new Parse.Query(Users);
    query.equalTo("objectId", this.currentUser);
    query.first({
      success: function (object) {
        var profilePhoto = object.get("ProfilePic");
        if (profilePhoto != undefined) { document.images[0].src = profilePhoto._url; }
        vm.name = object.attributes.name;
        vm.age = object.attributes.Age;
        vm.gender = object.attributes.Gender;
        vm.email = object.attributes.email;
        vm.username = object.attributes.username;
        // Successfully retrieved the object.
      },
      error: function (error) {
        alert("Error: " + error.code + " " + error.message);
      }
    });
  }

}
