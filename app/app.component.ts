import { Component } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NgModel } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Observable } from "rxjs/Rx";
import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { AutoCompleteModule } from 'primeng/autocomplete';
import { AutoComplete } from 'primeng/autocomplete';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ViewChild } from '@angular/core';
import { DashboardComponent } from './Dashboard/dashboard.component';
import { HomeComponent } from './Home/home.component';

@Component({
    moduleId: module.id,
    selector: 'my-app',
    template: `<router-outlet></router-outlet>`
})
@Injectable()
export class AppComponent {
}