"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var app_component_1 = require("./app.component");
var ag_grid_angular_1 = require("ag-grid-angular");
describe('GridComponent Tests', function () {
    var gridComponent;
    var fixture;
    // if you're not using templateUrls you can use something like this all in one beforeEach
    // beforeEach(() => {
    //     TestBed.configureTestingModule({
    //         imports: [AgGridModule.withComponents([])], // import the AgGridModule
    //         declarations: [AppComponent],              // declare the test component
    //     });
    //
    //     fixture = TestBed.createComponent(AppComponent);
    //
    //     gridComponent = fixture.componentInstance;      // AppComponent test instance
    // });
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            imports: [ag_grid_angular_1.AgGridModule.withComponents([])],
            declarations: [app_component_1.AppComponent],
        }).compileComponents(); // compile template and css
    }));
    beforeEach(function () {
        fixture = testing_1.TestBed.createComponent(app_component_1.AppComponent);
        gridComponent = fixture.componentInstance; // AppComponent test instance
    });
    it('grid API is not available until  `detectChanges`', function () {
        expect(gridComponent.gridOptions.api).not.toBeTruthy();
    });
    it('grid API is available after `detectChanges`', function () {
        fixture.detectChanges();
        expect(gridComponent.gridOptions.api).toBeTruthy();
    });
    it('select all button selects all rows', function () {
        fixture.detectChanges();
        gridComponent.selectAllRows();
        expect(gridComponent.gridOptions.api.getSelectedNodes().length).toEqual(3);
    });
});
//# sourceMappingURL=app.component.spec.js.map