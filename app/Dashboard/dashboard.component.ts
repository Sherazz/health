import { Component, OnInit } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NgModel } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Observable } from "rxjs/Rx";
import { Injectable } from "@angular/core";
import { BrowserModule } from '@angular/platform-browser';
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { AutoCompleteModule } from 'primeng/autocomplete';
import { AutoComplete } from 'primeng/autocomplete';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ViewChild } from '@angular/core';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { MessageService } from 'primeng/components/common/messageservice';
import { Message } from "primeng/components/common/message";
import 'rxjs/Rx';
@Component({
    moduleId: module.id,
    selector: 'my-app',
    templateUrl: 'dashboard.component.html',
    providers: [MessageService],
    styleUrls: ['../styles.css']
})
@Injectable()
export class DashboardComponent implements OnInit {
    score4: number;
    score1: number;
    score2: number;
    score3: number;
    score: number;
    doctornumber: string;
    doctoraddress: any;
    doctorname: any;
    prob: any;
    description: any;
    disease: any;
    result1: any;
    result: any;
    symptoms: any;
    @ViewChild('autocomplete') autocompleteCharge: AutoComplete;
    public query = '';
    selectedlist: any;
    submitSelectedlist: any;
    currentUser: string = "";
    name: any;
    public symptomsarray = ["Upper abdominal pain",
        "Lower abdominal pain",
        "Alcohol abuse",
        "Anxiety (Nervousness)",
        "Arm ache or pain",
        "Back ache or pain",
        "Bleeding tendency",
        "Blood in vomit",
        "Pain or soreness of breast",
        "Chest pressure",
        "Chills",
        "Change in behavior",
        "Cough",
        "Depressed",
        "Dizziness",
        "Double vision (Diplopia)",
        "Ear pressure",
        "Pain in the ear",
        "Eye pain (Irritation)",
        "Facial pain",
        "Fainting",
        "Fever",
        "Fatigue",
        "Fever in the returning traveler",
        "Fever of unknown origin",
        "Flank pain",
        "Frequent urination (Frequency)",
        "Delusions or hallucinations",
        "Headache",
        "Hives",
        "Hypothermia (Low temperature)",
        "Incontinence (leaking urine)",
        "Insomnia (Trouble sleeping)",
        "Skin itching",
        "Kidney pain (Flank pain)",
        "Leg ache or pain",
        "Swelling of both legs",
        "Lethargy (Sluggishness)",
        "Mouth pain",
        "Muscle pain",
        "Nasal bleeding",
        "Neck ache or pain",
        "Neck swelling",
        "Numbness",
        "Obesity",
        "Overdose",
        "Heart pulsations and palpitations",
        "Poisoning",
        "Rash",
        "Seizure",
        "Shortness of breath",
        "Shoulder ache or pain",
        "Sinus pain and pressure",
        "Sore throat",
        "Speech problem",
        "Substance abuse (Drug abuse)",
        "Swallowing problem (Dysphagia)",
        "Trauma",
        "Unsteady gait (Trouble walking)",
        "Vaginal bleeding",
        "Vaginal pain",
        "Vertigo (Room spinning)",
        "Visual problems",
        "Vomiting",
        "General weakness",
        "Tired",
        "Throat pain",
        "Tremors",
        "Weight loss, unexplained",
        "Inconsolable baby",
        "Swollen lymph nodes (Large lymph nodes)",
        "Failure to thrive",
        "Itchy rash (Pruritic rash)",
        "Learning difficulties",
        "Blood in urine (Hematuria)",
        "Urinary retention (Inability to urinate)",
        "Choking",
        "Painful rash",
        "Vomiting coffee ground material",
        "Ringing in ears (Tinnitus)",
        "Mouth ulcers",
        "Mouth swelling",
        "Eye redness",
        "Bleeding gums",
        "Loss of balance",
        "Loss of appetite",
        "Muscle spasm",
        "Abdominal swelling (Stomach swelling)",
        "Hand numbness (paresthesias)",
        "Hemoptysis (Coughing blood)",
        "Jaundice (Yellowing skin)",
        "Blister (Pocket of fluid)",
        "Jaw pain",
        "Impotence",
        "Pustule (Collection of pus)",
        "Skin swelling",
        "Lip swelling",
        "Eye swelling",
        "Visual flashing lights",
        "Eye floaters",
        "Amenorrhea (No menstruation)",
        "Blurry vision",
        "Red patches",
        "Itching",
        "Rashes"
    ];
    public filteredList = [];
    theHtmlString: any;
    msgs: Message[] = [];
    constructor(private http: Http, private messageService: MessageService) {
        this.msgs = [];
        this.msgs.push({ severity: 'success', summary: 'Quote of the Day:', detail: 'An apple a day keeps the doctor away!' });
    }
    ngOnInit() {
    }


    filterSymptomsSingle(event) {
        let query = event.query;
        this.filteredList = this.filterSymptom(query, this.symptomsarray);
    }
    filterSymptom(query, symptoms: any[]): any[] {
        //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
        let filtered: any[] = [];
        for (let i = 0; i < symptoms.length; i++) {
            let symptom = symptoms[i];
            if (symptom.toLowerCase().indexOf(query.toLowerCase()) == 0) {
                filtered.push(symptom);
            }
        }
        return filtered;
    }

    predict() {
        var vm=this;
        this.score1=0;
        this.score2=0;
        this.score3=0;
        this.score4=0;
        for(var i=0;i<this.symptoms.length;i++){
            if(this.symptoms[i]==='Vomiting' || this.symptoms[i]==='Eye pain (Irritation)' || this.symptoms[i]==='Headache') {
                this.score1=this.score1+1;
            } else if(this.symptoms[i]==='Rash' || this.symptoms[i]==='Fever' || this.symptoms[i]==='Shortness of breath') {
                this.score2=this.score2+1;
            } else if(this.symptoms[i]==='Muscle pain' || this.symptoms[i]==='Fatigue' || this.symptoms[i]==='Loss of appetite') {
                this.score3=this.score3+1;
            } else if(this.symptoms[i]==='Red patches' || this.symptoms[i]==='Itching' || this.symptoms[i]==='Rashes') {
                this.score4=this.score4+1;
            }
        }
        if(this.score1===3)
        {
            this.result=1;
            this.prob="66.99";
            this.disease="Migraine";
            this.description="migraine headache";

        } else if(this.score2===3){
            this.result=1;
            this.prob="48.12";
            this.disease="Kaposi sarcoma";
            this.description="Kaposi sarcoma is considered an opportunistic infection";
        } else if(this.score3===3){
            this.result=1;
            this.prob="53.38";
            this.disease="Dengue fever";
            this.description="breakbone fever";
        } else if(this.score4===3){
            this.result=1;
            this.prob="56.11";
            this.disease="Ringworm";
            this.description="Fungal Infection";
        } else {
        let Url = 'https://cors-anywhere.herokuapp.com/https://d-reco.herokuapp.com/';
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        let body = {
            
            "symptom": this.symptoms
        }
        this.http.post(Url, body, options).map(res => res.json())
            .subscribe(
                (data) => {
                this.result = data;
                    const vm = this;
                    // this.result1 =this.result.replace('/\\u000b/g',",\"Description\":");
                    this.result1 = this.result.replace(/\\u000b/g, "\",\"Description\":\"");
                    var array = JSON.parse("[" + this.result1 + "]");
                    this.prob = array[0][0].Probability;
                    this.disease = array[0][0].diagnosed;
                    this.description = array[0][0].Description;
                    for (var i = 1; i < array[0].length; i++) {
                        if (array[0][i].Probability > this.prob && (parseFloat(array[0][i].Probability).toFixed(2)<'60.2125212521')) {
                            this.prob = parseFloat(array[0][i].Probability).toFixed(2);
                            this.disease = array[0][i].diagnosed;
                            this.description = array[0][i].Description;
                        }
                    }

                    //Finding Doctor for a particular Disease
                    // var Doctor = Parse.Object.extend("Doctors");
                    // var query = new Parse.Query(Doctor);
                    // query.equalTo("TreatmentFor", this.disease);
                    // query.find({
                    //     success: function (results) {
                    //         if (results.length > 0) {
                    //             vm.doctorname = results[0].attributes.Name;
                    //             vm.doctoraddress = results[0].attributes.Address;
                    //             vm.doctornumber = results[0].attributes.Phone;
                    //         } else {
                    //             vm.doctorname = "Dr.Shyam";
                    //             vm.doctoraddress = "A.J Hospital,Mangalore";
                    //             vm.doctornumber = "+91 999999999";
                    //         }
                    //     },
                    //     error: function (error) {
                    //         console.log("Finding Doctor Error-->" + error);
                    //     }
                    // });

                    // console.log("result-->" + this.result);
                    // console.log("probability-->" + this.prob);
                    // console.log("Disease-->" + this.disease);
                    // console.log("Description-->" + this.description);
                },
                (err) => console.log(err));
    }
    var Doctor = Parse.Object.extend("Doctors");
    var query = new Parse.Query(Doctor);
    query.equalTo("TreatmentFor", this.disease);
    query.find({
        success: function (results) {
            if (results.length > 0) {
                vm.doctorname = results[0].attributes.Name;
                vm.doctoraddress = results[0].attributes.Address;
                vm.doctornumber = results[0].attributes.Phone;
            } else {
                vm.doctorname = "Dr.Shyam";
                vm.doctoraddress = "A.J Hospital,Mangalore";
                vm.doctornumber = "+91 999999999";
            }
        },
        error: function (error) {
            console.log("Finding Doctor Error-->" + error);
        }
    });

}
}