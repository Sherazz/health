"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var dashboard_component_1 = require("../Dashboard/dashboard.component");
var home_component_1 = require("../Home/home.component");
var doctors_component_1 = require("../Doctors/doctors.component");
var profile_component_1 = require("../profile/profile.component");
exports.routes = [
    {
        path: 'home', component: home_component_1.HomeComponent
    },
    {
        path: 'dashboard', component: dashboard_component_1.DashboardComponent
    },
    {
        path: 'doctors', component: doctors_component_1.DoctorsComponent
    },
    {
        path: '', component: home_component_1.HomeComponent, pathMatch: "full"
    },
    {
        path: 'profile', component: profile_component_1.ProfileComponent
    }
];
//# sourceMappingURL=app.routes.js.map