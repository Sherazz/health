import { Routes } from '@angular/router';
import { AppComponent } from '../app.component';
import { DashboardComponent } from '../Dashboard/dashboard.component';
import { HomeComponent } from '../Home/home.component';
import { DoctorsComponent } from '../Doctors/doctors.component';
import { ProfileComponent } from '../profile/profile.component';


export const routes: Routes = [
	{
		path: 'home', component: HomeComponent
	},
	{
		path: 'dashboard', component: DashboardComponent
	},
	{
		path: 'doctors', component: DoctorsComponent
	},
	{
		path: '', component: HomeComponent, pathMatch: "full"
	},
	{
		path: 'profile', component: ProfileComponent
	}
];