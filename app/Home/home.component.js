"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var HomeComponent = (function () {
    function HomeComponent(http, router) {
        this.http = http;
        this.router = router;
        this.myref = this;
        this.currentUser = "";
        this.error = "";
        this.erroremail = "";
        this.loginerror = "";
    }
    HomeComponent.prototype.login = function () {
        this.loginerror = "";
        var vm = this;
        Parse.User.logIn(this.user, this.pass, {
            success: function (user) {
                console.log("user logged in");
                vm.currentUser = Parse.User.current();
                localStorage.setItem('currentUser', vm.currentUser.id);
                $("#login-modal").modal("hide");
                vm.router.navigate(['/dashboard']);
                // Do stuff after successful login.
            },
            error: function (user, error) {
                vm.loginerror = "Invalid Username/Password";
                console.log("check username/password");
                // The login failed. Check error to see why.
            }
        });
    };
    HomeComponent.prototype.onChange = function (event) {
        console.log('onChange');
        this.files = event.srcElement.files;
        console.log(this.files);
    };
    HomeComponent.prototype.signup = function () {
        this.error = "";
        this.erroremail = "";
        var vm = this;
        var user = new Parse.User();
        user.set("name", this.name);
        user.set("Gender", this.gender);
        user.set("Age", this.age);
        user.set("username", this.username);
        user.set("password", this.password);
        user.set("email", this.email);
        var fileUploadControl = this.files;
        if (fileUploadControl != undefined) {
            var len = fileUploadControl.length;
            if (len > 0) {
                var file = fileUploadControl[0];
                var name = "photo.jpg";
                var parseFile = new Parse.File(name, file);
                user.set("ProfilePic", parseFile);
            }
        }
        user.signUp(null, {
            success: function (user) {
                // $("#signupForm").reset();
                $("#signup-modal").modal("hide");
                $("#login-modal").modal("hide");
                $("#signupsucessfull-modal").modal("show");
                vm.error = "";
                vm.erroremail = "";
                // Hooray! Let them use the app now.
                console.log("user created success");
            },
            error: function (user, error) {
                if (error.code == 125) {
                    vm.erroremail = "Invalid Email";
                }
                else {
                    vm.error = "Username not Available";
                }
                // Show the error message somewhere and let the user try again.
                // alert("Error: " + error.code + " " + error.message);
            }
        });
    };
    HomeComponent.prototype.successfull = function () {
        this.registration = "Sign Up Successfull! Please Log In !";
    };
    HomeComponent.prototype.errormsgreset = function () {
        this.loginerror = "";
    };
    return HomeComponent;
}());
HomeComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'my-app',
        templateUrl: 'home.component.html'
    }),
    core_2.Injectable(),
    __metadata("design:paramtypes", [http_1.Http, router_1.Router])
], HomeComponent);
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=home.component.js.map