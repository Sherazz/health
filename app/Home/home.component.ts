import { Component } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NgModel } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Observable } from "rxjs/Rx";
import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { AutoCompleteModule } from 'primeng/autocomplete';
import { AutoComplete } from 'primeng/autocomplete';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ViewChild } from '@angular/core';
import { Router } from "@angular/router";
@Component({
  moduleId: module.id,
  selector: 'my-app',
  templateUrl: 'home.component.html'
})

@Injectable()
export class HomeComponent {
  files: any;
  user: any;
  pass: any;
  registration: any;
  myref = this;
  currentUser: string = "";
  error: string = "";
  erroremail: string = "";
  loginerror: string = "";
  constructor(private http: Http, private router: Router) {
  }

  login() {
    this.loginerror = "";
    const vm = this;
    Parse.User.logIn(this.user, this.pass, {
      success: function (user) {
        console.log("user logged in");
        vm.currentUser = Parse.User.current();
        localStorage.setItem('currentUser', vm.currentUser.id);
        $("#login-modal").modal("hide");
        vm.router.navigate(['/dashboard']);
        // Do stuff after successful login.
      },
      error: function (user, error) {
        vm.loginerror = "Invalid Username/Password";
        console.log("check username/password");
        // The login failed. Check error to see why.
      }
    });
  }

  onChange(event) {
    console.log('onChange');
    this.files = event.srcElement.files;
    console.log(this.files);
  }
  signup() {
    this.error = "";
    this.erroremail = "";
    const vm = this;
    var user = new Parse.User();
    user.set("name", this.name);
    user.set("Gender", this.gender);
    user.set("Age", this.age);
    user.set("username", this.username);
    user.set("password", this.password);
    user.set("email", this.email);
    var fileUploadControl = this.files;
    if (fileUploadControl != undefined) {
      var len = fileUploadControl.length;
      if (len > 0) {
        var file = fileUploadControl[0];
        var name = "photo.jpg";

        var parseFile = new Parse.File(name, file);
        user.set("ProfilePic", parseFile);
      }
    }
    user.signUp(null, {
      success: function (user) {
        // $("#signupForm").reset();
        $("#signup-modal").modal("hide");
        $("#login-modal").modal("hide");
        $("#signupsucessfull-modal").modal("show");
        vm.error = "";
        vm.erroremail = "";
        // Hooray! Let them use the app now.
        console.log("user created success");
      },
      error: function (user, error) {
        if (error.code == 125) {
          vm.erroremail = "Invalid Email";
        }
        else {
          vm.error = "Username not Available";
        }
        // Show the error message somewhere and let the user try again.
        // alert("Error: " + error.code + " " + error.message);
      }
    });
  }
  successfull() {
    this.registration = "Sign Up Successfull! Please Log In !";
  }

  errormsgreset() {
    this.loginerror = "";
  }
}