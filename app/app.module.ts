import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { NgModel } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from "./app.component";
import { AutoCompleteModule } from 'primeng/autocomplete';
import { AccordionModule } from 'primeng/accordion';
import { MenuItem } from 'primeng/api';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ViewChild } from '@angular/core';
import { routes } from './Route/app.routes';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from './Dashboard/dashboard.component';
import { HomeComponent } from './Home/home.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { DoctorsComponent } from "./Doctors/doctors.component";
import { ProfileComponent } from "./profile/profile.component";
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { Authentication } from './authentication.component';

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        CommonModule, FormsModule, AutoCompleteModule, AccordionModule, BrowserAnimationsModule, RouterModule.forRoot(routes), MessageModule, MessagesModule
    ],
    declarations: [
        AppComponent, DashboardComponent, HomeComponent, SidenavComponent, DoctorsComponent, ProfileComponent
    ],
    providers: [
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
