import { Component, OnInit } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['../styles.css']
})
export class SidenavComponent implements OnInit {

  currentUser: any;
  name: any;
  constructor() { }

  ngOnInit() {
    const vm = this;
    vm.currentUser = localStorage.getItem('currentUser');
    var Users = Parse.Object.extend("User");
    var query = new Parse.Query(Users);
    query.equalTo("objectId", this.currentUser);
    query.first({
      success: function (object) {
        vm.name = object.attributes.name;
        // Successfully retrieved the object.
      },
      error: function (error) {
        alert("Error: " + error.code + " " + error.message);
      }
    });
  }
  logout() {
    Parse.User.logOut().then(() => {
      var currentUser = Parse.User.current();  // this will now be null
    });
    localStorage.removeItem('currentUser');
  }
}
