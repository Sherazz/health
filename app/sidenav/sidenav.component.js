"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var SidenavComponent = (function () {
    function SidenavComponent() {
    }
    SidenavComponent.prototype.ngOnInit = function () {
        var vm = this;
        vm.currentUser = localStorage.getItem('currentUser');
        var Users = Parse.Object.extend("User");
        var query = new Parse.Query(Users);
        query.equalTo("objectId", this.currentUser);
        query.first({
            success: function (object) {
                vm.name = object.attributes.name;
                // Successfully retrieved the object.
            },
            error: function (error) {
                alert("Error: " + error.code + " " + error.message);
            }
        });
    };
    SidenavComponent.prototype.logout = function () {
        Parse.User.logOut().then(function () {
            var currentUser = Parse.User.current(); // this will now be null
        });
        localStorage.removeItem('currentUser');
    };
    return SidenavComponent;
}());
SidenavComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'app-sidenav',
        templateUrl: './sidenav.component.html',
        styleUrls: ['../styles.css']
    }),
    __metadata("design:paramtypes", [])
], SidenavComponent);
exports.SidenavComponent = SidenavComponent;
//# sourceMappingURL=sidenav.component.js.map