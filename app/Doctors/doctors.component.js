"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var DoctorsComponent = (function () {
    function DoctorsComponent() {
        this.diseases = ["Abdominal trauma", "Abrasions", "ACE inhibitor induced cough", "Acute angle closure glaucoma", "Alcohol ethanol intoxication", "Back trauma", "Bacterial dysentery", "Bacterial infection", "Baker's cyst", "Barrett's esophagus", "Basilar skull fracture", "Cancer", "Candida albicans", "Carbon monoxide poisoning", "Cavity", "Dehydration", "Dengue fever", "Dental abscess", "Dentures", "Ear laceration", "Ear swelling", "Encephalitis", "Facial laceration", "Facial trauma", "Fat embolism", "Femoral artery aneurysm", "Gastric ulcer", "Genital herpes", "Glaucoma", "Goiter", "Head trauma", "Headache", "Hearing loss", "Heat rash", "Inhalants abuse", "Insulin overdose", "Interstitial cystitis", "Jet lag", "Keratitis", "Kidney cancer", "Knee laceration", "Laxative abuse", "Lead overdose", "Leg laceration", "Malaria", "Mallet finger", "Malnutrition", "Narcotic", "Nasal cancer", "Neck trauma", "Oral ulcers", "Orbital mass", "Paronychia", "Parotitis", "Patellar dislocation", "Quaternary syphilis", "Renal artery stenosis", "Renal failure", "Respiratory failure", "Self-inflicted injury", "Shigellosis", "Shoulder joint dislocation", "Tarantula bite", "Temporal arteritis", "Urinary tract malformation", "Urticaria", "Vasculitis", "Vasovagal syncope", "Whiplash injury", "Whooping cough", "Windshield wiper fluid"];
    }
    DoctorsComponent.prototype.ngOnInit = function () {
    };
    DoctorsComponent.prototype.filterDiseasesSingle = function (event) {
        var query = event.query;
        this.filteredList = this.filterDiseases(query, this.diseases);
    };
    DoctorsComponent.prototype.filterDiseases = function (query, diseases) {
        //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
        var filtered = [];
        for (var i = 0; i < diseases.length; i++) {
            var disease = diseases[i];
            if (disease.toLowerCase().indexOf(query.toLowerCase()) == 0) {
                filtered.push(disease);
            }
        }
        return filtered;
    };
    DoctorsComponent.prototype.search = function () {
        var vm = this;
        var Doctor = Parse.Object.extend("Doctors");
        var query = new Parse.Query(Doctor);
        query.equalTo("TreatmentFor", vm.disease);
        query.find({
            success: function (results) {
                if (results.length > 0) {
                    vm.doctorname = results[0].attributes.Name;
                    vm.doctoraddress = results[0].attributes.Address;
                    vm.doctornumber = results[0].attributes.Phone;
                }
                else {
                    // vm.doctorname = "Dr.Shyam";
                    // vm.doctoraddress = "A.J Hospital,Mangalore";
                    // vm.doctornumber = "+91 999999999";
                    alert("Record Not Found");
                }
            },
            error: function (error) {
                console.log("Finding Doctor Error-->" + error);
            }
        });
    };
    return DoctorsComponent;
}());
DoctorsComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'my-app',
        templateUrl: './doctors.component.html',
        styleUrls: ['../styles.css']
    }),
    __metadata("design:paramtypes", [])
], DoctorsComponent);
exports.DoctorsComponent = DoctorsComponent;
//# sourceMappingURL=doctors.component.js.map