import { Component, OnInit } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'my-app',
  templateUrl: './doctors.component.html',
  styleUrls: ['../styles.css']
})
export class DoctorsComponent implements OnInit {
  submitSelectedlist: any;
  selectedlist: any;
  filteredList: any[];
  doctorname: any;
  doctoraddress: any;
  doctornumber: any;
  disease: any;
  public diseases = ["Abdominal trauma", "Abrasions", "ACE inhibitor induced cough", "Acute angle closure glaucoma", "Alcohol ethanol intoxication", "Back trauma", "Bacterial dysentery", "Bacterial infection", "Baker's cyst", "Barrett's esophagus", "Basilar skull fracture", "Cancer", "Candida albicans", "Carbon monoxide poisoning", "Cavity", "Dehydration", "Dengue fever", "Dental abscess", "Dentures", "Ear laceration", "Ear swelling", "Encephalitis", "Facial laceration", "Facial trauma", "Fat embolism", "Femoral artery aneurysm", "Gastric ulcer", "Genital herpes", "Glaucoma", "Goiter", "Head trauma", "Headache", "Hearing loss", "Heat rash", "Inhalants abuse", "Insulin overdose", "Interstitial cystitis", "Jet lag", "Keratitis", "Kidney cancer", "Knee laceration", "Laxative abuse", "Lead overdose", "Leg laceration", "Malaria", "Mallet finger", "Malnutrition", "Narcotic", "Nasal cancer", "Neck trauma", "Oral ulcers", "Orbital mass", "Paronychia", "Parotitis", "Patellar dislocation", "Quaternary syphilis", "Renal artery stenosis", "Renal failure", "Respiratory failure", "Self-inflicted injury", "Shigellosis", "Shoulder joint dislocation", "Tarantula bite", "Temporal arteritis", "Urinary tract malformation", "Urticaria", "Vasculitis", "Vasovagal syncope", "Whiplash injury", "Whooping cough", "Windshield wiper fluid"];
  constructor() { }

  ngOnInit() {
  }
  filterDiseasesSingle(event) {
    let query = event.query;
    this.filteredList = this.filterDiseases(query, this.diseases);
  }
  filterDiseases(query, diseases: any[]): any[] {
    //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
    let filtered: any[] = [];
    for (let i = 0; i < diseases.length; i++) {
      let disease = diseases[i];
      if (disease.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(disease);
      }
    }
    return filtered;
  }

  search() {
    const vm = this;
    var Doctor = Parse.Object.extend("Doctors");
    var query = new Parse.Query(Doctor);
    query.equalTo("TreatmentFor", vm.disease);
    query.find({
      success: function (results) {
        if (results.length > 0) {
          vm.doctorname = results[0].attributes.Name;
          vm.doctoraddress = results[0].attributes.Address;
          vm.doctornumber = results[0].attributes.Phone;
        } else {
          // vm.doctorname = "Dr.Shyam";
          // vm.doctoraddress = "A.J Hospital,Mangalore";
          // vm.doctornumber = "+91 999999999";
          alert("Record Not Found");
        }
      },
      error: function (error) {
        console.log("Finding Doctor Error-->" + error);
      }
    });
  }

}
