import { CanActivate, Router } from '@angular/router';
import { HomeComponent } from './Home/home.component';
import { Injectable } from "@angular/core";
@Injectable()
export class Authentication implements CanActivate {

constructor(private service: HomeComponent, private router: Router) {}


canActivate() {
var a = localStorage.getItem('currentUser');
    if (a) { return true; }
    this.router.navigate(['/home']);
    return false;
  }
}